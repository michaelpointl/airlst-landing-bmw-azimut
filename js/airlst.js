Array.prototype.indexOf || (Array.prototype.indexOf = function(d, e) {
    var a;
    if (null == this) throw new TypeError('"this" is null or not defined');
    var c = Object(this),
        b = c.length >>> 0;
    if (0 === b) return -1;
    a = +e || 0;
    Infinity === Math.abs(a) && (a = 0);
    if (a >= b) return -1;
    for (a = Math.max(0 <= a ? a : b - Math.abs(a), 0); a < b;) {
        if (a in c && c[a] === d) return a;
        a++
    }
    return -1
});
customDirectives = angular.module('customDirectives', []);
customDirectives.directive('customPopover', function () {
    return {
        restrict: 'A',
        template: '<span>{{label}}</span>',
        link: function (scope, el, attrs) {
            scope.label = 'i';
            $(el).popover({
                trigger: 'outsideClick',
                html: true,
                content: attrs.popoverHtml,
                placement: attrs.popoverPlacement
            });
        }
    };
});

angular.module('app', [
	'oitozero.ngSweetAlert',
    'customDirectives',
    'ui.bootstrap'
])
.directive('ngEnter', [
	function() {
		return function(scope, element, attrs) {
			element.bind("keydown keypress", function(event) {
				if (event.which === 13) {
					scope.$apply(function() {
						scope.$eval(attrs.ngEnter);
					});

					event.preventDefault();
				}
			});
		};
	}
])

.controller('CountdownController', ['$scope', '$timeout',

    function($scope, $timeout) {

        $scope.countdown = {};
        $scope.countdown.days = "";
        $scope.countdown.hours = "";
        $scope.countdown.minutes = "";

        $scope.calc_diff = function(){

            var current_year = new Date().getFullYear();
            var current_month = new Date().getMonth()+1;
            var current_day = new Date().getDate();
            var current_hour = new Date().getHours();
            var current_min = new Date().getMinutes();

            var d1 = new Date(current_year,current_month,current_day,current_hour,current_min,0); // time now
            var d2 = new Date(2018,6,09,18,0,0); // wiesn - 13.02.2017 18:00:00

            var diff=d2-d1,sign=diff<0?-1:1,milliseconds,seconds,minutes,hours,days;
            diff/=sign; // or diff=Math.abs(diff);
            diff=(diff-(milliseconds=diff%1000))/1000;
            diff=(diff-(seconds=diff%60))/60;
            diff=(diff-(minutes=diff%60))/60;
            days=(diff-(hours=diff%24))/24;

            $scope.countdown = {};
            $scope.countdown.days = days;
            $scope.countdown.hours = hours;
            $scope.countdown.minutes = minutes;

        }

        $scope.calc_diff();

        $timeout(function () {
           $scope.calc_diff();
        }, 60000); // update time after 60 seconds

    }

])

.controller('ContactFormController', ['$scope','$http', 'SweetAlert',

    function($scope, $http, SweetAlert) {
    	var vm = this;
    	vm.model = {
    		sex: ''
    	};
        vm.errormsg = "";
        vm.message_sent = false;

        vm.resetForm = function(){
        	vm.model = {
        		sex: ''
        	};
        	$scope.contactform.$setPristine();
        }

        vm.contactFormSubmit = function(){

        	//if(form && !form.$valid)
			if(!$scope.contactform.$valid)
			{
				//form.$submitted = true;
				SweetAlert.swal('Fehlende Daten', 'Bitte alle benötigten Felder ausfüllen.', 'info');
				return;
			}

            var complete = true;

            if(vm.model.message == ""){
                complete = false;
                vm.errormsg = "Bitte vor dem Senden eine Nachricht eingeben.";
            }

            if(complete){
                $http({
                    url: 'https://wrapper.airlst.com/mail-eandv-day',
                    method: "POST",
                    data: { 
                        'message' : vm.model.message,
                        'sex': vm.model.sex,
                        'first_name' : vm.model.firstname,
                        'last_name': vm.model.lastname,
                        'email': vm.model.email,
                        'phone': vm.model.phone
                     }
                })
                .then(function(response) {
                        // success
                        vm.message_sent = true;
                        vm.resetForm();
                        $('#contactForm').modal('toggle');
                        SweetAlert.swal('Vielen Dank', 'Ihre Anfrage wurde erfolgreich versandt.', 'success');
                }, 
                function(response) { // optional
                        // failed
                        alert('Ihre Nachricht konnte nicht gesendet werden. Bitte wenden Sie sich per E-Mail an Ihren Ansprechpartner');
                        vm.resetForm();
                        $('#contactForm').modal('toggle');
                });
            }
        }

    }
])

.controller('AirLSTCtrl', ['$scope', '$http', '$filter', '$location', 'SweetAlert', '$sce', function($scope, $http, $filter, $location, SweetAlert, $sce) {

    $scope.popoverContent1 = $sce.trustAsHtml('I consent that BMW AG (Bayerische Motoren Werke Aktiengesellschaft, Petuelring 130, 80788 München) shall process and use personal data I have specified (for example contact details and personal data such as date of birth and class of driver\'s license, hobbies and interests) for customer care and selected promotional communication of information regarding products and services of the BMW Group as well as for market research. BMW AG may also pass on my data to BMW Sales Companies in my home country as well as individual contracted dealers and workshops (partners I have specified, partners where I have purchased vehicles or used services or consultancy services and/or my regional partner) who will use my data for the purposes stated and are also permitted to contact me in this regard. Updates may be passed on to the companies stated.');
    $scope.popoverContent2 = $sce.trustAsHtml('I can revoke the declaration of consent given to BMW AG at any time with effect for the future via BMW Customer Care (+49 89 1250-16000 or <a href="mailto:kundenbetreuung@bmw.de">kundenbetreuung@bmw.de</a>). Moreover, I can use the above communications channels to request information at any time regarding data of mine that are stored at BMW AG as well as the correction, deletion or blocking of my personal details. More details regarding the processing of my personal data by BMW AG as well as regarding my rights can be found in the <a href="#" target="_blank">data protection information</a> of BMW AG or BMW National Sales companies in my home market.');

    // Configuration
	$scope.rsvplist_id = undefined;
	$scope.segment = undefined;
	$scope.stripe_key = undefined;
	$scope.hostname = 'http://wrapper.airlst.dev';

	// VM
	$scope.rsvp_code = "";
	$scope.contact = {};
	$scope.rsvp = {};
	$scope.guests = [];
	
	$scope.partners = [];
	$scope.invitedPartners = [];

	$scope.previous_contact = {};
	$scope.previous_rsvp = {};
	$scope.status = ''; // confirm, cancel
	$scope.precheckin = '';

	$scope.toa_hour = '';
	$scope.tod_hour = '';
	$scope.toa_minute = '';
	$scope.tod_minute = '';

	$scope.lcb_year = '';
	$scope.lcb_month = '';

	$scope.teilnahme_abfrage = '';
	$scope.validation = '';

	// Helpers
	$scope.tab = 'form'; // code, form, summary, done, cancel, cancel_done
	$scope.submission_initiated = false;
	$scope.rsvp_mode = 'create'; // update, create
	$scope.limit = '';

	$scope.delete_previous_guests;

	$scope.form_step = 1;
	
	$scope.sending = false;

	$scope.general_lock = false;
	$scope.segment_lock = false;

	$scope.isWebkit = false;

	$scope.files = {};

	if(navigator.userAgent.indexOf('AppleWebKit') != -1){
		$scope.isWebkit = true;
	}

	$scope.loadRsvpCodeFromCookie = function () {
		var cookieValue = $cookies.get('airlst.rsvp_code');
		if(typeof cookieValue !== 'undefined') {
    		$scope.rsvp_code = cookieValue;
            $scope.rsvp_mode = 'update';

            $scope.loadRsvp();
    	}

    	 // $scope.addMitarbeiter();
    }

    $scope.removeRsvpCookie = function(){
		$cookies.remove('airlst.rsvp_code');
    }

    
	$scope.setStatus = function(status){
		$scope.status = status;
	}

	$scope.resetAllInputs = function(){
		
		$scope.delete_previous_guests = false;
		$scope.sending = false;
		$scope.rsvp = {};
	};

	$scope.showInitiativeInput = function(){

		$scope.rsvp_mode = 'create';
		$scope.status = 'accept';

		// Reference data
		$scope.contact = {};
		$scope.rsvp = {};
		$scope.previous_rsvp = angular.copy($scope.rsvp);
		$scope.previous_contact = angular.copy($scope.contact);

		$scope.toTab('registration');
	}

	$scope.showCodeInput = function(){

		$scope.rsvp_mode = 'update';


		// Reference data
		$scope.contact = {};
		$scope.rsvp = {};
		$scope.previous_rsvp = angular.copy($scope.rsvp);
		$scope.previous_contact = angular.copy($scope.contact);

		$scope.toTab('code');
	}

	$scope.gotoStepFromForm = function(step, form){
		
		if(form && !form.$valid){
			form.$submitted = true;
			SweetAlert.swal('Fehlende Daten', 'Bitte alle benötigten Felder ausfüllen.', 'info');
			return;
		}

		$scope.form_step = step;
	}

	$scope.loadRsvpCodeFromUrl = function(){

        var match = $location.absUrl().match(/rsvp_code=([A-Za-z0-9]+)/);
        var imprint = $location.absUrl().match(/imprint/);

        if((!$scope.guests || $scope.guests.length == 0)){
						$scope.addMitarbeiter();
					}

        if(match && match.length >= 2 && match[1]){
            $scope.rsvp_code = match[1];
            $scope.rsvp_mode = 'update';
            $scope.status = 'accept';

            $scope.loadRsvp();
        }

    	
    	if(imprint){
            $scope.tab ='imprint';
    	}


    }

    // $scope.loadimprintparamenterfromurl = function (){
    // 	 var match = $location.absUrl().match(/imprint/);
    // 	 $scope.tab = 'imprint'

    // }

	$scope.autocompleteFromMainContact = function(guest){
    	var fieldsToClone = [
    		'vorname',
    		'nachname',
    		'email',
    		'sex',
    		'titel',
    		'firma_position',
    		'contact_1'
    	];

		guest.contact[fieldsToClone] = $scope.contact[fieldsToClone];
    	angular.forEach(fieldsToClone, function(fieldsToClone) {
    		guest.contact[fieldsToClone] = $scope.contact[fieldsToClone];
    	});
    }
	
	// Functions
	$scope.loadRsvp = function(){
		var data = {
			'rsvplist_id' : $scope.rsvplist_id,
			'rsvp_code' : $scope.rsvp_code,
			'segment': $scope.segment
		};

		if($scope.rsvp_code == ""){
			SweetAlert.swal('Error', 'Your code isn´t valid.', 'error');
			return;
		}

		$http.get($scope.hostname+'/airlst/external/get_rsvp?rsvplist_id='+data.rsvplist_id+'&rsvp_code='+data.rsvp_code)
			.then(function(res){
				
				if(res.data && res.data.status == 100){
											
					// Reference data
					$scope.contact = res.data.payload.contact;
					$scope.rsvp = res.data.payload.rsvp;
					$scope.guests = res.data.payload.guests;

					
					if(res.data.payload.linked)
						$scope.invitedPartners = res.data.payload.linked;
					
					$scope.previous_rsvp = angular.copy($scope.rsvp);
					$scope.previous_contact = angular.copy($scope.contact);

					if($scope.contact.nachname == 'Name' ){
						$scope.contact.nachname = '';
					}

					if($scope.contact.sex == 'u' ){
						$scope.contact.sex = '';
					}

					// Clear Prefilled data
					// $scope.contact = {};
					// $scope.rsvp = {};
				

			

					// Set Mitarbeiter to Industrie 

					// Go to start
					$scope.toTab('form');

					// prevent angularjs to include an empty option in select
				
						
					
					// $scope.rsvp.rsvp_2 = "";
					// $scope.rsvp.rsvp_3 = "";
					// $scope.rsvp.rsvp_4 = "";
					// $scope.rsvp.rsvp_5 = "";
					// $scope.rsvp.rsvp_6 = "";
					// $scope.rsvp.rsvp_7 = "";
					// $scope.rsvp.rsvp_8 = "";
					// $scope.rsvp.rsvp_9 = "";
			

					// Tell the user
					// SweetAlert.swal('Ihr Code ist korrekt', 'Bitte registrieren Sie sich über das folgende Formular.', 'success');
				}
				else{
					// Clear data
					$scope.contact = {};
					$scope.rsvp = {};
					$scope.previous_rsvp = {};
					$scope.previous_contact = {};

					// Tell the user
					//SweetAlert.swal('Code nicht gefunden', 'Ihr Code ist nicht gültig. Bitte prüfen oder wenden Sie sich an Ihren Ansprechpartner.', 'error');
					$scope.limit = 'event_full'
				}
			}, function(err){
				SweetAlert.swal('Error', 'Eine Übertragung ist aus technischen Gründen nicht möglich. Bitte probieren Sie es noch einmal.', 'error');
			});
	}

	$scope.updateRsvp = function(){

		

		// Hotel für Gäste / Mitarbeiter
		if($scope.guests && $scope.guests.length > 0){
			angular.forEach($scope.guests, function(guest){

				guest.rsvp.status = 'confirmed';
			});
		}

		$scope.rsvp.rsvp_2 	= $scope.lcb_year + '/' + $scope.lcb_month;


		$scope.rsvp.status = 'confirmed';
		$scope.rsvp.pax_planned = '1';

		if($scope.contact.email != $scope.validation.email){
			SweetAlert.swal('Warning', 'Please check the email address you have entered.', 'error');
					$scope.sending = false;
					return;
					}

		

		var data = {
			rsvplist_id : $scope.rsvplist_id,
			rsvp_code : $scope.rsvp_code,
			contact: $scope.contact,
			rsvp: $scope.rsvp,
			guests: $scope.guests,
			delete_previous_guests: $scope.delete_previous_guests ? 'y' : 'n'
		};

		$scope.sending = true;


		$http.post($scope.hostname+'/airlst/external/update_rsvp', data)
			.then(function(res){
				
				if(res.data && res.data.status == 100){

					// Clear data
					// $scope.contact = {};
					// $scope.rsvp = {};
					// $scope.previous_rsvp = {};
					// $scope.previous_contact = {};

					// if($scope.contact.vorname == '--')
					// 	$scope.contact.vorname = '';

					// if($scope.contact.nachname == '--')
					// 	$scope.contact.nachname = '';

					// Tell the user
					SweetAlert.swal('Thank you', 'Thank you for registering your interest in the BMW 8 Series.', 'success');
					$scope.sending = false;
					$scope.delete_previous_guests = false;

					// Go to start
					$scope.toTab('done');
					
				}
				else{
					// Tell the user
					SweetAlert.swal('Fehler', ' 1 Eine Übertragung ist aus technischen Gründen nicht möglich. Bitte probieren Sie es noch einmal.', 'error');
					$scope.sending = false;
					
				}
			}, function(err){
				SweetAlert.swal('Fehler', ' 2 Eine Übertragung ist aus technischen Gründen nicht möglich. Bitte probieren Sie es noch einmal.', 'error');
				$scope.sending = false;
				
			});
	}

	$scope.createRsvp = function(){

		// $scope.rsvp.rsvp_7 		= $scope.toa_hour + ':' + $scope.toa_minute;
		// $scope.rsvp.rsvp_13 	= $scope.tod_hour + ':' + $scope.tod_minute;

		$scope.rsvp.rsvp_2 	= $scope.lcb_year + '/' + $scope.lcb_month;

	

		var items = [];

		items.push({
			contact: $scope.contact,
			rsvp: $scope.rsvp
		});


		var data = {
			rsvplist_id : $scope.rsvplist_id,
			rsvp_code : $scope.rsvp_code,
			items: items
		};

		$scope.sending = true;

		$http.post($scope.hostname+'/airlst/external/create_rsvp', data)
			.then(function(res){
				
				if(res.data && res.data.status == 100){

					// Clear data
					// $scope.contact = {};
					// $scope.rsvp = {};
					// $scope.previous_rsvp = {};
					// $scope.previous_contact = {};

					// Tell the user
					SweetAlert.swal('Thank you', 'Your registration was successful.', 'success');
					// SweetAlert.swal('Vielen Dank', 'Ihre Registrierung war erfolgreich.', 'success');
					$scope.sending = false;
					$scope.delete_previous_guests = false;

					// Go to start

					$scope.toTab('done');
				}
				else{
					// Tell the user
					$scope.sending = false;
				
					SweetAlert.swal('Fehler', 'Eine Übertragung ist aus technischen Gründen nicht möglich. Bitte probieren Sie es noch einmal.', 'error');
				}
			}, function(err){
				$scope.sending = false;

				SweetAlert.swal('Fehler', 'Eine Übertragung ist aus technischen Gründen nicht möglich. Bitte probieren Sie es noch einmal.', 'error');
			});
	}

	$scope.cancelRsvp = function(){

		var data = {
			rsvplist_id : $scope.rsvplist_id,
			rsvp_code : $scope.rsvp_code
		};

		$http.post($scope.hostname+'/airlst/external/cancel_rsvp', data)
			.then(function(res){
				
				if(res.data && res.data.status == 100){

					// Clear data
					$scope.contact = {};
					$scope.invoice_contact = {};
					$scope.rsvp = {};
					$scope.previous_rsvp = {};
					$scope.previous_contact = {};
					$scope.cc = {};

					// Tell the user
					SweetAlert.swal('Vielen Dank', 'Ihre Abmeldung wurde entgegengenommen.', 'success');

					// Go to start
					$scope.toTab('done');
				}
				else{
					// Tell the user
					SweetAlert.swal('Fehler', ' 5 Eine Übertragung ist aus technischen Gründen nicht möglich. Bitte probieren Sie es noch einmal.', 'error');
				}
			}, function(err){
				SweetAlert.swal('Fehler', '  6 Eine Übertragung ist aus technischen Gründen nicht möglich. Bitte probieren Sie es noch einmal.', 'error');
			});
	}

	$scope.addMitarbeiter = function(){
		$scope.guests.push({
			contact: {},
			rsvp: {}
		});
	}

	$scope.removeMitarbeiter = function(guest){

		var index = $scope.guests.indexOf(guest);

		if(index > -1){
			$scope.guests.splice(index, 1);
			$scope.delete_previous_guests = true;
		}

		// if($scope.guests.length == 0){
		// 	$scope.addMitarbeiter();
		// }
	}

	$scope.valEmail = function(){
		if($scope.contact.email != $scope.validation.email){
			SweetAlert.swal('Warning', 'Please check the email address you have entered', 'error');
					$scope.sending = false;
					return;
					}
		$scope.step = 'step2';
	}
	

	$scope.toInteger = function(value){
		return parseInt(value);
	}

	

	$scope.toTab = function(tab){
		$scope.tab = tab;
		
		// $scope.$apply(function(){
		// 	window.scrollTo(0,0);
		// });
		
		if(tab == 'done'){
			// $scope.resetAllInputs();
		}
	}

	$scope.submitRegistration = function(){

		if($scope.rsvp_mode == 'update'){
			$scope.updateRsvp();
		}
		else if($scope.rsvp_mode == 'create'){
			$scope.createRsvp();
		}
	}

	$scope.cancelRegistration = function(){
		$scope.cancelRsvp();
	}

	$scope.checkAndSubmit = function(){

		$scope.submission_initiated = true;

		//if(form && !form.$valid)
		if(!$scope.registration.$valid)
		{
			// form.$submitted = true;
			SweetAlert.swal('Error', 'Please complete all required fields.', 'info');
			return;
		}
		if(!$scope.rsvp.rsvp_5 && !$scope.rsvp.rsvp_6 && !$scope.rsvp.rsvp_7 && !$scope.rsvp.rsvp_4){
			SweetAlert.swal('Error', 'Please select min. 1 preferred contact method', 'info');
			return;
		}

		// if(form){
		// 	$scope.rsvp.rsvp_20 = 'completed';
		// }

		$scope.submitRegistration();
	}

	$scope.checkLimit = function(){

		$http.get($scope.hostname+'/airlst/external/limit_generic?rsvplist_id='+$scope.rsvplist_id+'&segment=confirmed')
			.then(function(res){
				
				if(res.data && res.data.status == 100){
					$scope.limits = res.data.payload;
				}
			});

	};

	$scope.checkAndProceedToSummary = function(form){
		
		if(form && !form.$valid)
		{
			form.$submitted = true;
			SweetAlert.swal('Fehlende Daten', 'Bitte alle benötigten Felder ausfüllen.', 'info');
			return;
		}	

		if(form){
			$scope.rsvp.rsvp_20 = 'completed';
		}

		$scope.toTab('start');
	}


}]);